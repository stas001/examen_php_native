-- --------------------------------------------------------
-- Хост:                         127.0.0.1
-- Версия сервера:               10.3.13-MariaDB-log - mariadb.org binary distribution
-- Операционная система:         Win32
-- HeidiSQL Версия:              10.1.0.5464
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Дамп структуры базы данных examen
CREATE DATABASE IF NOT EXISTS `examen` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `examen`;

-- Дамп структуры для таблица examen.carts
CREATE TABLE IF NOT EXISTS `carts` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(50) DEFAULT NULL,
  `Price` double DEFAULT NULL,
  `Massa` double DEFAULT NULL,
  `Picture` varchar(50) DEFAULT NULL,
  `Data_add` timestamp NULL DEFAULT current_timestamp(),
  `id_user` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `id_user` (`id_user`),
  CONSTRAINT `FK_carts_users` FOREIGN KEY (`id_user`) REFERENCES `users` (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы examen.carts: ~1 rows (приблизительно)
/*!40000 ALTER TABLE `carts` DISABLE KEYS */;
INSERT INTO `carts` (`ID`, `Name`, `Price`, `Massa`, `Picture`, `Data_add`, `id_user`) VALUES
	(11, 'Greck', 90, 375, '2.jpg', '2019-12-12 22:45:58', 2);
/*!40000 ALTER TABLE `carts` ENABLE KEYS */;

-- Дамп структуры для таблица examen.categoria
CREATE TABLE IF NOT EXISTS `categoria` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Categoria` varchar(50) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='#About categoria';

-- Дамп данных таблицы examen.categoria: ~2 rows (приблизительно)
/*!40000 ALTER TABLE `categoria` DISABLE KEYS */;
INSERT INTO `categoria` (`ID`, `Categoria`) VALUES
	(1, 'Pizza'),
	(2, 'Soup'),
	(3, 'Salat');
/*!40000 ALTER TABLE `categoria` ENABLE KEYS */;

-- Дамп структуры для таблица examen.products
CREATE TABLE IF NOT EXISTS `products` (
  `ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `Name` varchar(50) DEFAULT NULL,
  `Price` double DEFAULT NULL,
  `Massa` double DEFAULT NULL,
  `Picture` varchar(50) DEFAULT NULL,
  `id_c` int(11) DEFAULT NULL,
  `Date_create` timestamp NULL DEFAULT current_timestamp(),
  `Date_modif` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`ID`),
  KEY `Cat_id` (`id_c`),
  CONSTRAINT `FK_food_categoria` FOREIGN KEY (`id_c`) REFERENCES `categoria` (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8 COMMENT='#About food';

-- Дамп данных таблицы examen.products: ~2 rows (приблизительно)
/*!40000 ALTER TABLE `products` DISABLE KEYS */;
INSERT INTO `products` (`ID`, `Name`, `Price`, `Massa`, `Picture`, `id_c`, `Date_create`, `Date_modif`) VALUES
	(2, 'Piperoni', 90, 650, '3.jpg', 1, '2019-10-22 20:51:23', '2019-10-22 20:51:23'),
	(3, 'Greck', 90, 375, '2.jpg', 3, '2019-10-22 20:51:23', '2019-10-22 20:51:23');
/*!40000 ALTER TABLE `products` ENABLE KEYS */;

-- Дамп структуры для таблица examen.users
CREATE TABLE IF NOT EXISTS `users` (
  `ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `Login` varchar(50) DEFAULT NULL,
  `Password` varchar(50) DEFAULT NULL,
  `Name` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8 COMMENT='#About users';

-- Дамп данных таблицы examen.users: ~4 rows (приблизительно)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`ID`, `Login`, `Password`, `Name`) VALUES
	(1, 'admin@admin.com', 'admin', 'ADMIN'),
	(2, 'dima@dima.com', 'dima', 'Dima'),
	(4, 'victor@victor.com', 'vitor', 'Victor'),
	(5, 'g@g', 'g', 'g');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
